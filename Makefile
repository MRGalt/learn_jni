CFLAGS =  -I$(JAVA_HOME)/include -I$(JAVA_HOME)/include/darwin
LDFLAGS = -Wl,-undefined -Wl,dynamic_lookup -lpthread
CLASS_PATH = zyf/test
CFILE_PATH = jni
BUILD_PATH = build
PACK_NAME = $(subst /,., $(CLASS_PATH))
JAVA_FILES = $(notdir $(wildcard $(CLASS_PATH)/*.java))
TARGETS = $(patsubst %.java, %, $(JAVA_FILES))

default: targets

$(TARGETS):
	@mkdir -p $(BUILD_PATH)
	@clang $(CFLAGS) -c $(CFILE_PATH)/$@.c -o $(BUILD_PATH)/$@.o
	@clang $(LDFLAGS) -o $(BUILD_PATH)/lib$@.dylib $(BUILD_PATH)/$@.o
	@javac $(CLASS_PATH)/$@.java -d $(BUILD_PATH)
	cd $(BUILD_PATH) && java -Djava.library.path=. $(PACK_NAME).$@

all: $(TARGETS)

clean:
	rm -rf $(BUILD_PATH) 2> /dev/null || true

targets:
	@echo "可构建的目标列表: "$(TARGETS)
