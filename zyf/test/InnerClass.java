/**
 * Desc: 演示如何使用内部类
 * - 如何使用javap
 * Auth: 张宇飞
 * Date: 2017-05-15    
 */
package zyf.test;

public class InnerClass {
    static {
        System.loadLibrary("InnerClass");
    }

    public static native void startCpp();

    /*********************| 内部对象类 |*********************/
    public class Sparrow {
        public void tweet() {
            System.out.println("I'm 小麻雀。吱吱吱……");
        }
    }

    /*********************| 内部的静态类 |*********************/
    public static class Swallow {
        public static void tweet() {
            System.out.println("I'm 小燕子。唧唧唧……");
        }
    }
    
    /*********************| main |*********************/
    public static void main(String[] args) {
        System.out.println("----------------------| 演示如何在 Cpp 中访问内部类 |----------------------");
        InnerClass.startCpp();

        // 创建内部静态类的对象
        //InnerClass.Swallow swallow = new InnerClass.Swallow();

        //// 创建内部实例类的对象
        //InnerClass cls = new InnerClass();
        //InnerClass.Sparrow sparrow = cls.new Sparrow();
    }
}

