/**
 * Desc: 演示如何在 C 中传递复杂数据结构到 Java
 * Auth: 张宇飞
 * Date: 2017-05-15    
 */

package zyf.test;

public class PassStruct {
    static {
        // 加载动态库
        System.loadLibrary("PassStruct");
    }

    public static class Hand {
        int fingerNum;
        String name;
    };

    long what;
    Hand hand;

    public native static boolean getStructFromC(PassStruct st);

    public void dump() {
        System.out.println("what is " + what);
        System.out.format("hand's name is %s, fingerNum = %d\n", hand.name, hand.fingerNum);
    }

    /*********************| main |*********************/
    public static void main(String[] args) {
        System.out.println("----------------------| 演示C中传递复杂结构体给Java |----------------------");
        PassStruct st;
        if (PassStruct.getStructFromC(st)) {
            st.dump();
        }
    }
}

