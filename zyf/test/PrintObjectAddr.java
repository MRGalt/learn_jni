package zyf.test;

public class PrintObjectAddr {
    static {
        System.loadLibrary("PrintObjectAddr");
    }

    native public void hello();
    native public void hi();

    void handleNotify(int d) {
        System.out.println("receive notify: " + d);
    }

    public static void main(String[] args) {
        PrintObjectAddr p = new PrintObjectAddr();
        p.hello();
        p.hi();
    } 
}
