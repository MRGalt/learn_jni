/**
 * Desc: 展示Cpp 和 Java 的相互调用
 * 1. 如何加载动态库
 * 2. 如何使用javah
 * 3. GetMethodID报错
 *
 * Auth: 张宇飞
 * Date: 2017-05-15    
 */

package zyf.test;

public class CallFunc {
    static {
        // 加载动态库
        System.loadLibrary("CallFunc");
    }

    // 一个native函数
    public native void hiCpp();

    // 这个函数将在C中被调用
    private void giveMeFive(int num) {
        System.out.println("得到一个数字: " + num);
    }

    // 这个静态函数将在C中被调用
    private static void staticFunc(final String msg) {
        System.out.println("这是一个私有的静态函数, 传递过来的消息: " + msg);
    }

    /*********************| main |*********************/
    public static void main(String[] args) {
        System.out.println("----------------------| 演示Cpp和Java的相互调用 |----------------------");
        CallFunc c = new CallFunc();
        c.hiCpp();
    }
}

