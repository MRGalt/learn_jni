/**
 * Desc: 演示 JNI 多线程的情况下 FindClass 异常的情况
 * - JNIOnLoad 的使用
 * Auth: 张宇飞
 * Date: 2017-05-15    
 */

package zyf.test;
public class NoClass {
    static {
        System.loadLibrary("NoClass");
    }
    
    public native static void startCpp();

    // bello 小黄人的hello
    public static void bello() {
        System.out.println("Java say: Bello C !");
        ClassLoader l = Thread.currentThread().getContextClassLoader();
        if (l != null) {
            System.out.println("current thread classloader : " + l.toString());
        } else {
            System.out.println("current thread no classloader");
        }
    }

    /*********************| main |*********************/
    public static void main(String[] args) {
        System.out.println("----------------------| 演示找不到类的异常 |----------------------");
        NoClass.startCpp();
    }
}
