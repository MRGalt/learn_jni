#include <stdio.h>
#include "zyf_test_InnerClass.h"

static void tweet(JNIEnv* env, jclass cls, jobject obj) {
    if (obj) {
        // 实例函数
        jmethodID func = (*env)->GetMethodID(env, cls, "tweet", "()V");
        if (func) {
            (*env)->CallVoidMethod(env, obj, func);
        }
    } else {
        // 静态函数
        jmethodID func = (*env)->GetStaticMethodID(env, cls, "tweet", "()V");
        if (func) {
            (*env)->CallStaticVoidMethod(env, cls, func);
        }
    }
}

void JNICALL Java_zyf_test_InnerClass_startCpp(JNIEnv* env, jclass cls) {
    // 获取内部静态类
    jclass swallowCls = (*env)->FindClass(env, "zyf/test/InnerClass$Swallow");
    if (swallowCls) {
        tweet(env, swallowCls, NULL);
        (*env)->DeleteLocalRef(env, swallowCls);
    }

    // 获取内部实例类
    jmethodID innerClassInit = (*env)->GetMethodID(env, cls, "<init>", "()V");
    if (!innerClassInit) { return; }
    jobject innerClassObj = (*env)->NewObject(env, cls, innerClassInit);
    if (!innerClassObj) { return; }

    jclass sparrowCls = (*env)->FindClass(env, "zyf/test/InnerClass$Sparrow");

    if (sparrowCls) {
        jmethodID sparrowInit = (*env)->GetMethodID(env, sparrowCls, "<init>", "(Lzyf/test/InnerClass;)V");
        if (sparrowInit) {
            jobject sparrow = (*env)->NewObject(env, sparrowCls, sparrowInit, innerClassObj);
            if (sparrow) {
                tweet(env, sparrowCls, sparrow);
                (*env)->DeleteLocalRef(env, sparrow);
            }
        }
        (*env)->DeleteLocalRef(env, sparrowCls);
    }

    (*env)->DeleteLocalRef(env, innerClassObj);
}
