#include <stdio.h>
#include "zyf_test_CallFunc.h"

void JNICALL Java_zyf_test_CallFunc_hiCpp(JNIEnv* env, jobject obj) {
    fprintf(stderr, "hello Cpp\n");

    jclass cls = (*env)->GetObjectClass(env, obj);
    if (!cls) return;

    // 调用java的 giveMeFive 函数
    jmethodID func = (*env)->GetMethodID(env, cls, "giveMeFive", "(I)V");
    if (func) {
        (*env)->CallVoidMethod(env, obj, func, 5);
    }

    // 调用java的静态函数
    func = (*env)->GetStaticMethodID(env, cls, "staticFunc", "(Ljava/lang/String;)V");
    if (func) {
        jstring jstr = (*env)->NewStringUTF(env, "拜拜 Java");
        if (jstr) {
            (*env)->CallStaticVoidMethod(env, cls, func, jstr);
            (*env)->DeleteLocalRef(env, jstr);
        }
    }

    (*env)->DeleteLocalRef(env, cls);
}
