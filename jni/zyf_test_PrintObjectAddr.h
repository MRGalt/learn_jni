/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class zyf_test_PrintObjectAddr */

#ifndef _Included_zyf_test_PrintObjectAddr
#define _Included_zyf_test_PrintObjectAddr
#ifdef __cplusplus
extern "C" {
#endif
/*
 * Class:     zyf_test_PrintObjectAddr
 * Method:    hello
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_zyf_test_PrintObjectAddr_hello
  (JNIEnv *, jobject);

/*
 * Class:     zyf_test_PrintObjectAddr
 * Method:    hi
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_zyf_test_PrintObjectAddr_hi
  (JNIEnv *, jobject);

#ifdef __cplusplus
}
#endif
#endif
