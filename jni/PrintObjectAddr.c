#include <stdio.h>
#include "zyf_test_PrintObjectAddr.h"

struct Cache {
    JNIEnv* env;
    jobject obj;
};

static struct Cache cache;

void JNICALL Java_zyf_test_PrintObjectAddr_hello(JNIEnv * env, jobject obj) {
    printf("hello %p\n", obj);
    cache.env = env;
    cache.obj = obj;
}

void JNICALL Java_zyf_test_PrintObjectAddr_hi(JNIEnv * env, jobject obj) {
    printf("hi %p\n", obj);

    jclass cls = (*cache.env)->GetObjectClass(cache.env, cache.obj);
    if (!cls) return;

    // 调用java的 giveMeFive 函数
    jmethodID func = (*cache.env)->GetMethodID(cache.env, cls, "handleNotify", "(I)V");
    if (func) {
        (*cache.env)->CallVoidMethod(cache.env, cache.obj, func, 5);
    }

    (*cache.env)->DeleteLocalRef(cache.env, cls);

}
