#include <jni.h>
#include <android/log.h>

#define LOG_TAG "zyf.test"

static void dump_ref_table(JNIEnv* env, const char* id) {
    __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, "###########| %s |##########\n", id);
    jclass cls = env->FindClass("dalvik/system/VMDebug");
    jmethodID mid = env->GetStaticMethodID(cls, "dumpReferenceTables", "()V" );
    env->CallStaticVoidMethod(cls, mid);
    env->DeleteLocalRef(cls);
}

extern "C"
JNIEXPORT void JNICALL
Java_zyf_test_MainActivity_makeOverflow(JNIEnv* env, jobject obj) {
    dump_ref_table(env, "1");

    jclass cls = env->GetObjectClass(obj);
    dump_ref_table(env, "2");
//
    const char* hello = "Hello from C++";
    env->NewStringUTF(hello);
    dump_ref_table(env, "3");
//
//    // 制造一个栈溢出
//    for (int i = 0; i < 512; ++i) {
//        env->NewStringUTF(hello);
//    }
}
